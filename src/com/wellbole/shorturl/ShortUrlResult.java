/**
 *
 * Copyright (C) 2013 陕西威尔伯乐信息技术有限公司
 * http://www.wellbole.com
 *
 * @className:com.wellbole.shorturl.ShortUrlResult
 * @description: 短地址生成结果
 *
 * @version:v1.0.0
 * @author:李飞
 *
 * Modification History:
 * Date         Author      Version     Description
 * -----------------------------------------------------------------
 * 2013年10月1日      李飞              v1.0.0        create
 *
 */
package com.wellbole.shorturl;

/**
 * 短地址生成结果
 * @author 李飞
 */
public class ShortUrlResult {
	/**
	 * 状态代码,约定返回200为生成正常。
	 */
	private  int	statusCode;
	/**
	 * 状态说明
	 */
	private  String statusTxt;
	/**
	 * 原始长地址
	 */
	private  String longUrl;
	/**
	 * 生成的短地址
	 */
	private  String shortUrl;
	
	public boolean isOk(){
		return this.statusCode == 200;
	}
	
	public final int getStatusCode() {
		return statusCode;
	}
	public final void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public final String getStatusTxt() {
		return statusTxt;
	}
	public final void setStatusTxt(String statusTxt) {
		this.statusTxt = statusTxt;
	}
	public final String getLongUrl() {
		return longUrl;
	}
	public final void setLongUrl(String longUrl) {
		this.longUrl = longUrl;
	}
	public final String getShortUrl() {
		return shortUrl;
	}
	public final void setShortUrl(String shortUrl) {
		this.shortUrl = shortUrl;
	}
	@Override
	public String toString() {
		return "ShortUrlResult [statusCode=" + statusCode + ", statusTxt="
				+ statusTxt + ", longUrl=" + longUrl + ", shortUrl=" + shortUrl
				+ "]";
	}
}
